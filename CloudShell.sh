RESOURCEGROUP=ghost-vm-rg
LOCATION=eastus
az group create --name $RESOURCEGROUP --location $LOCATION
USERNAME=azureuser
PASSWORD=$(openssl rand -base64 32)
KEY=id_rsa.pub
DNS_LABEL_PREFIX=ghostly-$RANDOM
TEMPLATEURI=https://gitlab.com/retetr/azureghostdeploy/raw/master/azuredeploy.json

curl $TEMPLATEURI > azuredeploy.json

az deployment group validate \
  --resource-group $RESOURCEGROUP \
  --template-file azuredeploy.json \
  --parameters adminUsername=$USERNAME \
  --parameters adminPublicKey=$KEY \
  --parameters dnsLabelPrefix=$DNS_LABEL_PREFIX

az deployment group create \
  --name GhostDeployment \
  --resource-group $RESOURCEGROUP \
  --template-file azuredeploy.json \
  --parameters adminUsername=$USERNAME \
  --parameters authenticationType=password \
  --parameters adminPasswordOrKey=$PASSWORD \
  --parameters dnsLabelPrefix=$DNS_LABEL_PREFIX

az deployment group show \
  --name GhostDeployment \
  --resource-group $RESOURCEGROUP

az vm list \
  --resource-group $RESOURCEGROUP \
  --output table